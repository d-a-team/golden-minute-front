(function (window) {
  window.__env = window.__env || {};

  // API url
  //window.__env.apiUrl = 'http://localhost:2000/api/';
  window.__env.apiUrl = 'https://sbs.golden-minutes.com:2000/api/';

  //API url for connect to device
  //window.__env.apiUrlForDevice = 'http://localhost:3000/commands/';
  window.__env.apiUrlForDevice = 'https://sbs.golden-minutes.com:3000/commands/';

  //API url for connect to web socket
  //window.__env.webSocketUrl = 'http://localhost:3000';
  window.__env.webSocketUrl = 'https://sbs.golden-minutes.com:3000';


  //save phone number
  //save serial number
  //connect details 
  window.__env.phoneNumber = "+9720501234567";
  window.__env.serialNumber = "abc-123";


}(this));