import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { TenantSettingPageComponent } from './components/tenant/tenant-setting-page/tenant-setting-page.component';
import { HomePageForIntegratorComponent } from './components/integrator/home-page-for-integrator/home-page-for-integrator.component';
import { AddDevicePageComponent } from './components/integrator/add-device-page/add-device-page.component';


const routes: Routes = [
  {
    path: "",
    component: LoginPageComponent
  },
  {
    path: "home-page-for-integrator",
    component: HomePageForIntegratorComponent
  },
  {
    path: "home-page-for-tenant",
    component: TenantSettingPageComponent
  },
  /* {
    path: "setting-tenant-page",
    component: TenantSettingPageComponent
  }, */
  {
    path: "add-device-page",
    component: AddDevicePageComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
