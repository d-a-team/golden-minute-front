export interface SignData {
    deviceSerialNumber: string,
    buildingNumber: BuildingNumber,
    alarm: AlarmData,
    light: LightData,
    screenData: ScreenData,
    door: {
        status: false
    },
    gate: {
        status: false
    },
    wifi: {
        status: false,
        wifiName: "",
        defaultWifi: {
            password: ""
            wifiName: ""
        }
    }
}

export interface BuildingNumber {
    number: number,
    subText: string
}

export interface LightData {
    state: boolean,
    color: string,
    duration: number,
    blinkState: boolean,
    blinkTime: number,

    //DELETE
    type: 'A' | 'B' | 'C'
}

export interface AlarmData {
    state: boolean,
    duration: number
}

export interface ScreenData {
    type: string,
    id: number,
    queue: number,
    duration: number,

    text?: string,
    color?: string,
    fontSize?: string,
    font?: string,

    entranceAnimationType?: string,
    entranceAnimationDuration?: number,
    exitAnimationType?: string,
    exitAnimationDuration?: number,

    image?: string,
    video?: string,
    name?: string,
    size?: string
}

/* export interface TextsData {
    id: number,
    text: string,
    queue: number,
    duration: number,
    color?: string,
    fontSize?: string,
    font?: string,
    entranceAnimationType?: string,
    entranceAnimationDuration?: number,
    exitAnimationType?: string,
    exitAnimationDuration?: number,
}

export interface ImagesData {
    id: number,
    image: string,
    name: string,
    size: string,
    duration: number,
    queue: number,
    entranceAnimationType?: string,
    entranceAnimationDuration?: number,
    exitAnimationType?: string,
    exitAnimationDuration?: number
}

export interface VideoData {
    id: number,
    video: string,
    name: string,
    size: string,
    duration: number,
    queue: number
} */





/************************************/

export interface SignDataAlive {
    text: Text,
    buildingNumber: number,
    image: Image,
    video: Video,
    light: Light,
    alarm: boolean
}

export interface Text {
    text: string,
    id: number,
    duration: number
}
export interface Image {
    image: string,
    id: number,
    duration: number
}
export interface Video {
    video: string,
    id: number
}
export interface Light {
    color: string,
    state: boolean
}

export interface DataForBuildingNumberModal {
    number: number,
    subText: string
}

export interface DataForActiveLightModal {
    light: {
        state: boolean,
        color: string,
        duration: number,
        blinkState: boolean,
        blinkTime: number
    }
}

export interface DataForActiveAlarmModal {
    state: boolean,
    duration: number
}

export interface DataForCameraModal {
    status: boolean
}
export interface DataForWifiModal {
    status: boolean,
    wifiName: any
}