export interface UserData {
    userName: string;
    userId: string;
    userLevel: number;
    deviceSerialNumber: string;
}
