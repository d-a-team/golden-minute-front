export interface PersonalDetails {
    firstName: string;
    lastName: string;
    email: string;
    phone: string;

    floor: string;
    apartment: number;

    userName: string;
    password: string;
    passwordSecond: string;
}
