export interface FamilyDetails {
    name: string;
    sex: string;
    age: number;
    diseases:string;
    additions: string;
}