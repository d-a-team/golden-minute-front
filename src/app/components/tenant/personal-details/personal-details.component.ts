import { Component, OnInit } from '@angular/core';
import { PersonalDetails } from 'src/app/class/personalDetails';

@Component({
  selector: 'app-personal-details',
  templateUrl: './personal-details.component.html',
  styleUrls: ['./personal-details.component.css']
})
export class PersonalDetailsComponent implements OnInit {

  public personalDetails: PersonalDetails;

  public dataIsReady: boolean = false;
  public savingData: boolean = false;
  public loadingPage: boolean = false;


  constructor() {
    this.personalDetails = {
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      floor: "",
      apartment: null,
      userName: "",
      password: "",
      passwordSecond: ""
    };
  }

  ngOnInit() {
    this.dataIsReady = true;
  }

  //save the data of tenant
  public updateDetails() {
    this.personalDetails;

    this.savingData = true;
  }

}
