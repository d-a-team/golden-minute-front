import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-camera-modal',
  templateUrl: './camera-modal.component.html',
  styleUrls: ['./camera-modal.component.css']
})
export class CameraModalComponent implements OnInit {

  @Input() dataForCameraModal: any;
  @Output() closeCameraModal = new EventEmitter();

  constructor() { }

  ngOnInit() {

    this.openModal();
  }





  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#cameraModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#cameraModal').modal('hide');

    if (response)
      this.closeCameraModal.emit(response);
    else
      this.closeCameraModal.emit();
  }

}
