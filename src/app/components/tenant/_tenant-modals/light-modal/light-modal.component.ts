import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { MessageService } from 'primeng/api';
import { LightData } from 'src/app/class/smartsignData';
import { WebSocketService } from 'src/app/shared/services/web-socket.service';
import { EnvService } from 'src/app/shared/services/env/env.service';
declare var $: any;

@Component({
  selector: 'app-light-modal',
  templateUrl: './light-modal.component.html',
  styleUrls: ['./light-modal.component.css']
})
export class LightModalComponent implements OnInit {

  @Input() dataForActiveLightModal: any;
  @Output() closeActiveLightModal = new EventEmitter();

  public lightData: LightData;

  public showDeactivateLightbutton: boolean = false;
  public loading: boolean = false;

  constructor(public dataFromDeviceService: DataFromDeviceService,
    private messageService: MessageService,
    private webSocketService: WebSocketService,
    private envService: EnvService) { }

  ngOnInit() {
    //if the light active
    if (this.dataForActiveLightModal.light.state) {
      this.lightData = this.dataForActiveLightModal.light;
      this.showDeactivateLightbutton = true;
    }
    else {
      this.lightData = {
        state: false,
        color: "#FF0000",
        type: "A",
        duration: 30,
        blinkTime: 250,
        blinkState: false
      }

    }
    this.openModal();
  }


  //
  setActiveLight() {
    this.loading = true;
    this.lightData.state = true;

    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      light: this.lightData
    }

    //send data to smart-sign
    this.dataFromDeviceService.apiName = "activate-light";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'active light start', detail: "the light activate succesfuly" });

      this.closeModal(response);
    })
  }

  //
  deactiveLight() {
    this.loading = true;
    this.lightData.state = false;

    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      light: this.lightData
    }

    //send data to smart-sign
    this.dataFromDeviceService.apiName = "deactivate-light";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'active light stop', detail: "the light deactivate succesfuly" });

      this.closeModal(response)
    })
  }


  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#activeLightModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#activeLightModal').modal('hide');

    if (response)
      this.closeActiveLightModal.emit(response);
    else
      this.closeActiveLightModal.emit();
  }
}
