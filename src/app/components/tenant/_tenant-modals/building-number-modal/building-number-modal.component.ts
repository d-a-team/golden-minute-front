import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { MessageService } from 'primeng/api';
import { BuildingNumber } from 'src/app/class/smartsignData';
import { WebSocketService } from 'src/app/shared/services/web-socket.service';
import { EnvService } from 'src/app/shared/services/env/env.service';
declare var $: any;

@Component({
  selector: 'app-building-number-modal',
  templateUrl: './building-number-modal.component.html',
  styleUrls: ['./building-number-modal.component.css']
})
export class BuildingNumberModalComponent implements OnInit {

  @Input() dataForBuildingNumberModal: any;
  @Output() closeBuildingNumberModal = new EventEmitter();

  public buildingNumber: BuildingNumber;
  public loading: boolean = false;

  constructor(public dataFromDeviceService: DataFromDeviceService,
    private messageService: MessageService,
    private webSocketService: WebSocketService,
    private envService: EnvService) { }

  ngOnInit() {
    //update data
    this.buildingNumber = this.dataForBuildingNumberModal;

    //open modal
    this.openModal();
  }

  //
  saveBuildingNumberChange() {
    this.loading = true;

    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      buildingNumber: this.buildingNumber
    }

    //send data to smart-sign
    this.dataFromDeviceService.apiName = "set-building-number";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {
      this.messageService.add({ severity: 'success', summary: 'set building number', detail: "building number update succesfuly" });

      if (response.status)
        this.closeModal(this.buildingNumber);
      else
        this.closeModal(false);
    })
  }


  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#buildingNumberModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#buildingNumberModal').modal('hide');

    if (response)
      this.closeBuildingNumberModal.emit(response);
    else
      this.closeBuildingNumberModal.emit();
  }

}
