import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { AlarmData } from 'src/app/class/smartsignData';
import { WebSocketService } from 'src/app/shared/services/web-socket.service';
import { EnvService } from 'src/app/shared/services/env/env.service';
declare var $: any;

@Component({
  selector: 'app-alarm-modal',
  templateUrl: './alarm-modal.component.html',
  styleUrls: ['./alarm-modal.component.css']
})
export class AlarmModalComponent implements OnInit {

  @Input() dataForActiveAlarmModal: any;
  @Output() closeActiveAlarmModal = new EventEmitter();

  public timer = 3;
  public interval;

  public alarmData: AlarmData;

  public loading: boolean = false;

  constructor(public dataFromDeviceService: DataFromDeviceService,
    private messageService: MessageService,
    private webSocketService: WebSocketService,
    private envService: EnvService) { }

  ngOnInit() {

    if (this.dataForActiveAlarmModal.state) {
      this.alarmData = this.dataForActiveAlarmModal;
    }
    else {
      this.alarmData = {
        state: false,
        duration: 10
      }
      this.startAlarm();
    }


    this.openModal();
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
    this.interval = null;
  }

  //activiate the alarm
  private startAlarm() {
    this.interval = setInterval(async () => {
      if (this.timer > 0) {
        this.timer--;
      } else {
        clearInterval(this.interval);
        this.interval = null;
        this.callToSign();
      }
    }, 1000)
  }

  //cancel the alarm
  public cancelAlarm() {
    this.closeModal();
  }


  //callToSign
  private callToSign() {
    this.loading = true;
    this.alarmData.state = true;

    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      alarm: this.alarmData
    }

    //get status of smart sign from server
    this.dataFromDeviceService.apiName = "activate-alarm";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'activate alarm', detail: "Alarm was activate succesfuly" });

      this.closeModal(response);
    })
  }


  //
  public stopAlarm() {
    this.loading = true;
    this.alarmData.state = false;

    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      alarm: this.alarmData
    }

    //get status of smart sign from server
    this.dataFromDeviceService.apiName = "deactivate-alarm";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.closeModal(response);
    })
  }



  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#activeAlarmModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#activeAlarmModal').modal('hide');

    if (response)
      this.closeActiveAlarmModal.emit(response);
    else
      this.closeActiveAlarmModal.emit();
  }
}
