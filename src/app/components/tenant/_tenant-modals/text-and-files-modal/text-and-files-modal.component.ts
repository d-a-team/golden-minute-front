import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { ScreenData } from 'src/app/class/smartsignData';
import { SortablejsOptions } from "ngx-sortablejs";
import { WebSocketService } from 'src/app/shared/services/web-socket.service';
import { EnvService } from 'src/app/shared/services/env/env.service';
declare var $: any;

@Component({
  selector: 'app-text-and-files-modal',
  templateUrl: './text-and-files-modal.component.html',
  styleUrls: ['./text-and-files-modal.component.css']
})
export class TextAndFilesModalComponent implements OnInit {

  @Output() closeTextFileModal = new EventEmitter();

  public screanListData: ScreenData[] = [];
  public durationList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25, 30, 45, 60, 90, 120, 150, 180];
  public queueList: Number[] = [];

  public dataIsReady: boolean = false;
  public loading: boolean = false;

  constructor(private dataFromDeviceService: DataFromDeviceService,
    private webSocketService: WebSocketService,
    private envService: EnvService) { }

  ngOnInit() {
    this.openModal();

    //get the program list from service
    this.getDataFromServer();
  }


  //get program list from service
  public getDataFromServer() {
    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id
    }

    //get all data from server
    this.dataFromDeviceService.apiName = "get-program-list";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.screanListData = response ? response : [];
      this.updateQueueList();

      //data is ready
      this.dataIsReady = true;
    })
  }


  //open and close the card
  clickOpenCloseCard(row, index) {

    for (let item of this.screanListData)
      if (row.id != item.id)
        item['showItemCard'] = false;

    row['showItemCard'] = !row['showItemCard'];

    //go to card location
    let element = document.getElementById("rowCard_" + index);
    if (element)
      element.scrollIntoView();
  }

  //open card to select what to add
  public openAddRowModal() {
    $('#textFilesModal').modal('hide');
    $('#addRowModal').modal('show');
  }

  //open card to select what to add
  public closeAddRowModal() {
    $('#addRowModal').modal('hide');
    $('#textFilesModal').modal('show');
  }

  //add a new row to the screen
  public addRow(type) {
    this.closeAddRowModal();

    //EDIT - to real one
    let id = this.screanListData.length;
    //EDIT
    let queue = this.screanListData.length;

    if (type == "Text") {
      this.screanListData.push({
        type: "Text",
        id: id,
        queue: queue,
        duration: 5,
        text: "Text",
        color: "black",
        fontSize: "80",
        font: "serif",
        entranceAnimationType: 'normal',
        entranceAnimationDuration: 1,
        exitAnimationType: 'normal',
        exitAnimationDuration: 1
      })
    }
    else if (type == "Image") {
      this.screanListData.push({
        type: "Image",
        id: id,
        queue: queue,
        duration: 5,
        image: "",
        entranceAnimationType: 'normal',
        entranceAnimationDuration: 1,
        exitAnimationType: 'normal',
        exitAnimationDuration: 1
      })
    }
    if (type == "Video") {
      this.screanListData.push({
        type: "Video",
        id: id,
        queue: queue,
        duration: 5,
        video: "",
        entranceAnimationType: 'normal',
        entranceAnimationDuration: 1,
        exitAnimationType: 'normal',
        exitAnimationDuration: 1
      })
    }

    this.updateQueueList();

    this.clickOpenCloseCard(this.screanListData[this.screanListData.length - 1], this.screanListData.length - 1);
  }

  //delete row from the screen by the index
  public deleteRow(index) {
    this.screanListData.splice(index, 1);

    this.updateQueueList();
  }


  //update the num of element in the queue list array
  updateQueueList() {
    this.queueList = [];
    if (this.screanListData.length > 0)
      for (let i = 0; i < this.screanListData.length; i++) {
        this.queueList.push(i);
      }
  }


  //save changes
  public saveChanges() {
    this.loading = true
    for (let i = 0; i < this.screanListData.length; i++) {
      this.screanListData[i].id = i;
    }

    let dataToSend = {
      serialNumber: 'abc-123',
      socketId: this.webSocketService.ws.id,
      programList: this.screanListData
    };

    this.dataFromDeviceService.apiName = "set-program-list";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {
      this.loading = false;

      this.screanListData.forEach((item) => item.duration = Number(item.duration));
      this.screanListData.forEach((item) => item.entranceAnimationDuration = Number(item.entranceAnimationDuration));
      this.screanListData.forEach((item) => item.exitAnimationDuration = Number(item.exitAnimationDuration));
      this.screanListData.forEach((item) => item.queue = Number(item.queue));

      this.closeModal(this.screanListData);
    });
  }


  /***************************/
  //           Files         //
  /***************************/

  //open upload file
  public openUploadFile(type) {
    if (type == "image")
      document.getElementById("fileImageInput").click();
    else if (type == "video")
      document.getElementById("fileVideoInput").click();
  }


  //
  addImageUpload(event, index) {
    let tempFile: File = event.target.files[0];
    let myReader: FileReader = new FileReader();

    if (tempFile) {
      myReader.readAsDataURL(tempFile);
    }

    myReader.onloadend = (e) => {

      this.screanListData[index].image = <any>myReader.result;
      this.screanListData[index].name = tempFile.name;
      this.screanListData[index].size = tempFile.size.toString();

      return;
    }
  }

  //
  addVideoUpload(event, index) {

    let tempFile: File = event.target.files[0];
    let myReader: FileReader = new FileReader();

    if (tempFile) {
      myReader.readAsDataURL(tempFile);
    }

    myReader.onloadend = (e) => {

      this.screanListData[index].video = <any>myReader.result;
      this.screanListData[index].name = tempFile.name;
      this.screanListData[index].size = tempFile.size.toString();

      //get the video duration
      setTimeout(() => {
        var video = document.getElementById('videoId');
        var duration = video["duration"];
        this.screanListData[index].duration = Number(duration.toFixed(0)) + 1;
      }, 1000)

      return;
    }
  }



  /*********************************/
  //           Drag & Drop         //
  /*********************************/


  options: SortablejsOptions = {
    handle: '.handle',
    onEnd: (event: any) => {
      this.showFinalArray(event);
    }
  };

  showFinalArray(event) {
    this.screanListData.forEach((item, i) => item.queue = i);
    //console.log(this.screanListData); // מראה את המערך המעודכן
    // console.log(event.oldIndex); // מראה את המיקום הישן של האלמנט
    // console.log(event.newIndex); // מראה את המיקום החדש של האלמנט
  }

  //update to the new posision in the array
  updateQueue(index) {

    let item = this.screanListData[index];

    this.screanListData.splice(index, 1);
    this.screanListData.splice(item.queue, 0, item);

    this.screanListData.forEach((item, i) => item.queue = i);
  }


  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#textFilesModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#textFilesModal').modal('hide');

    if (response)
      this.closeTextFileModal.emit(response);
    else
      this.closeTextFileModal.emit();
  }
}