import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
declare var $: any;

@Component({
  selector: 'app-emergency-modal',
  templateUrl: './emergency-modal.component.html',
  styleUrls: ['./emergency-modal.component.scss']
})
export class EmergencyModalComponent implements OnInit {

  @Output() closeEmergencyModal = new EventEmitter();

  constructor(private dataFromDeviceService: DataFromDeviceService) { }

  ngOnInit() {

    this.openModal();
  }

  // send to the service to activae the emergency satate
  startEmegencyState() {
    let dataToSend = {
      type: "A",
      screen
    };

    //DELETE
    this.closeModal(dataToSend);
    //DELETE



    //get all data from server
    this.dataFromDeviceService.apiName = "active-emergency";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.closeModal(response);
    })
  }





  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#emergencyModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#emergencyModal').modal('hide');

    if (response)
      this.closeEmergencyModal.emit(response);
    else
      this.closeEmergencyModal.emit();
  }

}
