import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { WebSocketService } from 'src/app/shared/services/web-socket.service';
import { MessageService } from 'primeng/api';
import { EnvService } from 'src/app/shared/services/env/env.service';
import { isNgTemplate } from '@angular/compiler';
declare var $: any;

@Component({
  selector: 'app-wifi-modal',
  templateUrl: './wifi-modal.component.html',
  styleUrls: ['./wifi-modal.component.scss']
})
export class WifiModalComponent implements OnInit {

  @Input() dataForWifiModal: any;
  @Output() closeWifiModal = new EventEmitter();

  public wifiConnections = [];
  public selectedWifi;
  public dataIsReady: boolean = false;
  public loading: boolean = false;

  constructor(private dataFromDeviceService: DataFromDeviceService,
    private webSocketService: WebSocketService,
    private messageService: MessageService,
    private envService: EnvService) { }

  ngOnInit() {

    this.getDataFromServer();

    this.openModal();
  }

  //get wifi connection list from smart sign
  private getDataFromServer() {
    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id
    }

    //get all data from server
    this.dataFromDeviceService.apiName = "get-wifi";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {
      let wifiList = response.data.split("|");
      wifiList.splice(wifiList.length - 1, 1);

      for (let wifi of wifiList) {
        this.wifiConnections.push({
          wifiName: wifi.split("&")[0],
          powerful: wifi.split("&")[1],
          status: false
        })
      }

      if (this.dataForWifiModal.status) {
        for (let wifi of this.wifiConnections) {
          //show the connected network
          if (wifi.wifiName == this.dataForWifiModal.wifiName) {
            wifi.status = true;
          }

          //show the home network
          if (wifi.wifiName == this.dataForWifiModal.defaultWifi.wifiName) {
            wifi.homeNetwork = true;`1    `
            wifi.password = this.dataForWifiModal.defaultWifi.password;
          }
        }
      }

      //order the list by the power of the wifi
      this.wifiConnections.sort(function (a, b) {
        return b.powerful - a.powerful;
      });

      this.dataIsReady = true;
    })
  }

  //
  expendedRow(item) {

    //close all other
    for (let wifi of this.wifiConnections)
      wifi.expanded = false;

    this.selectedWifi = item;
    item.expanded = !item.expanded;
  }


  //connect to wifi
  public connectToWifi() {
    this.loading = true;

    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      connectionData: {
        wifi: this.selectedWifi
      }
    }

    //get all data from server
    this.dataFromDeviceService.apiName = "connect-wifi";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.loading = false;

      if (response.status) {
        let data = {
          status: response.status,
          wifi: this.selectedWifi
        }
        this.closeModal(data);
      }
      else
        this.messageService.add({ severity: 'info', summary: 'Error connection.', detail: "Wrong user name or password try again!!" });

    })
  }



  //
  public disConnect() {
    this.loading = true;

    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      connectionData: {
        wifi: this.selectedWifi
      }
    }

    //get all data from server
    this.dataFromDeviceService.apiName = "disconnect-wifi";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.loading = false;

      if (response.status) {
        this.selectedWifi.status = false;

        let data = {
          status: false,
          wifi: null
        }
        this.closeModal(data);
      }
      else
        this.messageService.add({ severity: 'info', summary: 'Error connection.', detail: "Wrong user name or password try again!!" });

    })
  }



  /***************************/
  //           Modal         //
  /***************************/

  //when open modal
  openModal() {
    $('#WifiModal').modal('show');
  }

  //when closing modal
  closeModal(response?) {
    $('#WifiModal').modal('hide');

    if (response)
      this.closeWifiModal.emit(response);
    else
      this.closeWifiModal.emit();
  }

}
