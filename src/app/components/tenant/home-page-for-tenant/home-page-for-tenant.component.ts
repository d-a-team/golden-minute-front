import { Component, OnInit, ViewChild } from '@angular/core';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { MessageService } from 'primeng/api';
import {
  DataForActiveAlarmModal, DataForActiveLightModal,
  DataForBuildingNumberModal, DataForCameraModal, SignData, DataForWifiModal
} from 'src/app/class/SmartSignData';
import { SignControllerComponent } from 'src/app/shared/components/sign-controller/sign-controller.component';
import * as io from 'socket.io-client';
import { WebSocketService } from 'src/app/shared/services/web-socket.service';
import { EnvService } from 'src/app/shared/services/env/env.service';

declare var $: any;

@Component({
  selector: 'app-home-page-for-tenant',
  templateUrl: './home-page-for-tenant.component.html',
  styleUrls: ['./home-page-for-tenant.component.css']
})
export class HomePageForTenantComponent implements OnInit {

  @ViewChild(SignControllerComponent, null) signControllerComponent: SignControllerComponent;

  public dataIsReady: boolean = false;
  public showAlarmCancelIcon: boolean = false;
  public loadingPage: boolean = true;
  public signStatus: boolean = false;
  public serviceStatus: boolean = false;

  public policeNumber = 100;
  public ambulanceNumber = 101;
  public tenantLevel: number = 2;
  public clickableSign = false;

  public signDataAlive: SignData = {
    deviceSerialNumber: "",
    buildingNumber: {
      number: null,
      subText: ""
    },
    alarm: {
      state: false,
      duration: 10
    },
    light: {
      state: false,
      color: "",
      duration: 10,
      blinkState: true,
      blinkTime: 250,

      type: "A"
    },
    screenData: {
      type: "",
      id: null,
      queue: null,
      duration: 0,
    },
    door: {
      status: false
    },
    gate: {
      status: false
    },
    wifi: {
      status: false,
      wifiName: "",
      defaultWifi: null
    }
  };

  constructor(private dataFromDeviceService: DataFromDeviceService,
    private messageService: MessageService,
    private webSocketService: WebSocketService,
    private envService: EnvService) { }

  async ngOnInit() {
    // this.messageService.add({ severity: 'info', summary: 'Connection.', detail: "Try start connect with the service!!" });

    this.dataIsReady = true;
    this.signControllerComponent;

    if (this.tenantLevel >= 2)
      this.clickableSign = true;

    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
    })

    //start web csocket
    let userSocketResponse = await this.startConnectionWebSocket();

    //if success coonect to websocket
    //get status data from sign
    if (userSocketResponse) {
      await this.getStatusDataFromServer();
      this.serviceStatus = true;
    }
    else {
      this.loadingPage = false;
      this.messageService.add({ severity: 'error', summary: 'Service not response.', detail: "we get error when try connect user socket!!" });
    }
  }


  ngOnDestroy(): void {
    //close the ws 

  }


  /***************************************************/
  /**********     web socket connection     **********/
  /***************************************************/
  public async startConnectionWebSocket() {

    let serialNumber = this.envService.serialNumber;
    let response = await this.webSocketService.connectToWebSocket(serialNumber);

    //if connect succsfully start lisining to event
    if (response["status"]) {
      this.serviceStatus = true;
      this.listiningToEvent(response["ws"]);
    }


    response["ws"].on('reconnecting', (number) => {
      console.log('Reconnecting to server - from home page of tenant', number);
      this.loadingPage = true;
    });

    //socket connection
    //when lose the connection with the service
    response["ws"].on('disconnect', () => {
      console.log('Disconnect from server - from home page of tenant');
      this.serviceStatus = false;
    });

    //when connect again
    response["ws"].on('connect', () => {
      console.log("Connected to websocket - from home page of tenant");
      this.serviceStatus = true;
      this.loadingPage = false;

      //if need to do it
      this.listiningToEvent(response["ws"]);
    });


    return response["status"];
  }

  //
  public listiningToEvent(ws) {
    ws.on('message', (message) => {
      console.log("Received: '" + JSON.stringify(message) + "'");

      switch (message.type) {

        //events from sign
        case "event": {
          switch (message.data.type) {
            //event of screen => text/img/video 
            case "Text":
            case "Image":
            case "Video": {
              console.log(new Date().toString() + "- new " + message.data.type + " event arrivaed from sign");

              this.updateScreen(message.data);
              break;
            }

            // event in alarm
            case "alarmState": {
              console.log("new event from alarm: " + message.data.status.state);

              this.updateAlarm(message.data.status);
              break;
            }

            // event in light
            case "lightState": {
              console.log("new event from light: " + message.data.status.state);

              this.updateLight(message.data.status);
              break;
            }

            // event in building number
            case "buildingNumber": {
              console.log("new event in building number");

              this.updateBuildingBumber(message.data.status);
              break;
            }

            // event in the connection or with the wifi connection
            case "connection": {

              console.log("new event in connection to internet");

              //when have connection with sign
              if (message.data.status.active) {
                this.signStatus = true;
                this.getStatusDataFromServer();
              }
              else
                this.signStatus = false;

              //when this is wifi connection
              if (message.data.status.wifi) {
                let wifiData = {
                  status: message.data.status.wifi,
                  wifiName: message.data.status.wifiName
                }
                this.updateWifi(wifiData);
              }
              else {
                let wifiData = {
                  status: false,
                  wifiName: null
                }
                this.updateWifi(wifiData);
              }

              break;
            }

            //other events
            default: {
              console.log("Other type");
              break;
            }
          }

          break;
        }

        //alarm that happend in sign
        case "alarm": {
          console.log("new alarm arrivaed from sign, message: " + message);
          break;
        }

        //other
        default: {
          console.log("Other type, message: " + message);
          break;
        }
      }
    });
  }



  /**************************************************/
  /**********     get data from server     **********/
  /**************************************************/
  private getStatusDataFromServer() {
    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id
    }

    //this.userDataService

    //get status of smart sign from server
    this.dataFromDeviceService.apiName = "get-status";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {
      this.loadingPage = false;
      this.signStatus = true;

      console.log(response);

      this.updateScreen(response.currentView);
      this.updateBuildingBumber(response.buildingNumber);
      this.updateLight(response.lightState);
      this.updateAlarm(response.alarmState);
      this.updateWifi(response.wifi);

      //this.signDataAlive.deviceSerialNumber = dataToSend.serialNumber;

    }, (ex) => {
      if (ex.error == "no connected clients")
        this.messageService.add({ severity: 'error', summary: 'Error in sign.', detail: "The sign not connected!!" });
      else
        this.messageService.add({ severity: 'error', summary: 'Error in sign.', detail: "error when try to get sign status !!,  Error:" + ex.error });

      this.signStatus = false;
      this.loadingPage = false;
      console.log(ex.message);
    })
  }


  /*************************************************************/
  /*****                   update screen                   *****/
  /*************************************************************/

  //
  updateScreen(newData) {
    this.signDataAlive.screenData = newData;

    if (this.signDataAlive.screenData != undefined) {
      //update text
      if (this.signDataAlive.screenData.type == "Text")
        this.signControllerComponent.setText(this.signDataAlive.screenData);
      //update image
      if (this.signDataAlive.screenData.type == "Image")
        this.signControllerComponent.setImage(this.signDataAlive.screenData);
      //update video
      if (this.signDataAlive.screenData.type == "Video")
        this.signControllerComponent.setVideo(this.signDataAlive.screenData);
    }
  }

  //
  updateLight(newData) {
    this.signDataAlive.light = newData;

    //update light color
    if (this.signDataAlive.light.state)
      this.signControllerComponent.setColor(this.signDataAlive.light);
    else
      this.signControllerComponent.setColor(this.signDataAlive.light);
  }


  //
  updateAlarm(newData) {
    this.signDataAlive.alarm = newData;

    //update alarm
    if (this.signDataAlive.alarm.state) {
      this.showAlarmCancelIcon = true;
      this.dataForActiveAlarmModal.state = true;
    }
    else {
      this.showAlarmCancelIcon = false;
      this.dataForActiveAlarmModal.state = false;
    }
  }


  //
  updateBuildingBumber(newData) {
    this.signDataAlive.buildingNumber = newData;

    //update building number
    this.signControllerComponent.setBuildingNum(this.signDataAlive.buildingNumber);
  }

  //update the status of the building door
  updateDoor(newData) {
    this.signDataAlive.door = newData;


  }

  //update the status of the parking space gate
  updateGate(newData) {
    this.signDataAlive.gate = newData;

  }


  //update the status of the wifi
  updateWifi(newData) {
    this.signDataAlive.wifi = newData;
  }














  /********************************************************************************/
  /*********************************** Actions  ***********************************/
  /********************************************************************************/

  //open the car gate
  public openCarGate() {
    //data to send
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      phoneNumber: this.envService.phoneNumber
    }

    //send request to the server to open the door gate
    this.dataFromDeviceService.apiName = "open-gate";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'open gate', detail: "the gate car open succesfully" });

    })
  }

  //open the building door
  public openBuildingDoor() {
    //data to send 
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id,
      phoneNumber: this.envService.phoneNumber
    }

    //send request to the server to open the door gate
    this.dataFromDeviceService.apiName = "open-door";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'open door', detail: "the building door open succesfuly" });

    })
  }

  //
  public callToAmbulance(number) {
    window.location.href = "tel:" + number;
  }

  //
  public callToPolice(number) {
    window.location.href = "tel:" + number;
  }

  //this function start alarm state in emargny case
  public aciveAmargncyState() {

    //will activate light alarm 
    //wil open building door
    //wil open gate in parking
    //will make call amergancy

  }

  //this function wil send sms by the sign
  sendSms() {
    //data to send 
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.socketId,
      smaData: {
        phoneNumber: "",
        messageText: ""
      }
    }

    //send request to the server to send sms
    this.dataFromDeviceService.apiName = "send-sms";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'send sms', detail: "sms sent succesfuly" });
    })
  }

  //this function will restart the sign
  restartSign() {
    //data to send 
    let dataToSend = {
      serialNumber: this.envService.serialNumber,
      socketId: this.webSocketService.ws.id
    }

    //send request to the server to restart the sign
    this.dataFromDeviceService.apiName = " restart-sign";
    this.dataFromDeviceService.saveData(dataToSend).subscribe((response: any) => {

      this.messageService.add({ severity: 'success', summary: 'Restart sign', detail: "the restart succesfuly" });

    })
  }










  /******************************************************************************/
  /*********************************** Modals ***********************************/
  /******************************************************************************/

  public showActiveLightModal: boolean = false;
  public showTextFilesModal: boolean = false;
  public showBuildingNumberModal: boolean = false;
  public showCameraModal: boolean = false;
  public showAlarmModal: boolean = false;
  public showWifiModal: boolean = false;

  public dataForBuildingNumberModal: DataForBuildingNumberModal = {
    number: null,
    subText: ""
  };
  public dataForActiveLightModal: DataForActiveLightModal = {
    light: {
      state: false,
      color: "",
      duration: null,
      blinkState: false,
      blinkTime: null
    }
  };
  public dataForActiveAlarmModal: DataForActiveAlarmModal = {
    state: false,
    duration: 0
  };

  public dataForCameraModal: DataForCameraModal = {
    status: false
  }

  public dataForWifiModal: DataForWifiModal = {
    status: false,
    wifiName: null
  }


  /***************************/
  //          Alarm          //
  /***************************/
  //function open alerm modal
  public openAlarmModal() {
    this.dataForActiveAlarmModal = this.signDataAlive.alarm;
    this.showAlarmModal = true;
  }

  //after close alarm modal
  public onCloseActiveAlarmModal(data) {
    this.showAlarmModal = false;
  }




  /***************************/
  //          Light          //
  /***************************/
  //function open light modal
  public openLightModal() {
    this.dataForActiveLightModal.light = this.signDataAlive.light;
    this.showActiveLightModal = true;
  }

  //after close light modal
  public onCloseLightModal(data) {
    this.showActiveLightModal = false;
  }




  /******************************/
  //          Building          //
  /******************************/
  //function open building number modal
  public openBuildingNumberModal() {
    this.dataForBuildingNumberModal = this.signDataAlive.buildingNumber;
    this.showBuildingNumberModal = true;
  }

  //after close building number modal
  public onCloseBuildingNumberModal(data) {
    this.showBuildingNumberModal = false;
  }




  /**********************************/
  //          Text & files          //
  /**********************************/
  //function open building number modal
  public openTextAndFilesModal() {
    this.showTextFilesModal = true;
  }

  //after close building number modal
  public onCloseTextAndFilesModal(data) {
    this.showTextFilesModal = false;
  }




  /****************************/
  //          Camera          //
  /****************************/
  //
  openCameraModal() {
    this.showCameraModal = true;
  }

  //after closing modal camera
  public onCloseCameraModal(data) {
    this.showCameraModal = false;
  }




  /**************************/
  //          Wifi          //
  /**************************/
  //
  openWifiModal() {
    this.showWifiModal = true;
    this.dataForWifiModal = this.signDataAlive.wifi;
  }

  //after closing modal camera
  public onCloseWifiModal(data) {
    this.showWifiModal = false;

    //after this nneds come from event
    //DELETE
    /*  this.signDataAlive.wifi.status = data.status;
     this.signDataAlive.wifi.wifiName = data.wifi.wifiName;
     this.signDataAlive.wifi.wifi = data.wifi; */
  }

}
