import { Component, OnInit } from '@angular/core';
import { FamilyDetails } from 'src/app/class/FamilyDetails';

@Component({
  selector: 'app-family-details',
  templateUrl: './family-details.component.html',
  styleUrls: ['./family-details.component.css']
})
export class FamilyDetailsComponent implements OnInit {


  public familyDetails: FamilyDetails[] = [];
  public dataIsReady: boolean = false;

  constructor() { }

  ngOnInit() {
    this.dataIsReady = true;
  }

  //
  addFamilyMember() {
    this.familyDetails.push({
      name: "",
      sex: "",
      age: null,
      diseases: "",
      additions: "",
    })
  }

  //
  updateFamilyData() {

  }

}
