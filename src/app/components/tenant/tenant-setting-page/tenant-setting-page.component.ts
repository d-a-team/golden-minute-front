import { Component, OnInit } from '@angular/core';
declare var $: any

@Component({
  selector: 'app-tenant-setting-page',
  templateUrl: './tenant-setting-page.component.html',
  styleUrls: ['./tenant-setting-page.component.css']
})
export class TenantSettingPageComponent implements OnInit {

  public computer: boolean = true;
  public showHomePage: boolean = true;
  public showPersonalDetails: boolean = false;
  public showFemilyDetails: boolean = false;
  public showContactUs: boolean = false;
  public showSmartSign: boolean = false;

  constructor() { }

  ngOnInit() {
    this.onResize();

    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
  }

  //this function called when the scren size changed
  onResize() {
    let width = $(window).width();
    if (width > 768) {
      this.computer = true;
    }
    else {
      this.computer = false;
    }
  }

  //open home-page
  //
  public openHomePage() {
    this.showHomePage = true;

    this.showPersonalDetails = false;
    this.showFemilyDetails = false;
    this.showSmartSign = false;
    this.showContactUs = false;
  }

  //
  public openPersonalDetails() {
    this.showPersonalDetails = true;

    this.showFemilyDetails = false;
    this.showSmartSign = false;
    this.showContactUs = false;
    this.showHomePage = false;
  }

  //
  public openFemilyDetails() {
    this.showFemilyDetails = true;

    this.showPersonalDetails = false;
    this.showSmartSign = false;
    this.showContactUs = false;
    this.showHomePage = false;
  }

  public openSmartSign() {
    this.showSmartSign = true;

    this.showPersonalDetails = false;
    this.showFemilyDetails = false;
    this.showContactUs = false;
    this.showHomePage = false;
  }

  public openContactUs() {
    this.showContactUs = true;

    this.showPersonalDetails = false;
    this.showFemilyDetails = false;
    this.showSmartSign = false;
    this.showHomePage = false;
  }


  //
  public openModalEditSmartSign() {

  }
}
