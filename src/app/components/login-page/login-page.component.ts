import { Component, OnInit } from '@angular/core';
import { LoginUserData } from 'src/app/class/LoginUserData';
import { Router } from '@angular/router';
import { DataService } from 'src/app/shared/services/data.service';
import { UserDataService } from 'src/app/shared/services/user-data.service';
import { CookiesService } from 'src/app/shared/services/cookies.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  public loginUserData: LoginUserData = {
    userName: "",
    password: "",
  };

  public serialLoginNumber: string;
  public rememberMe: boolean = false;
  public loginWithSerialNumber: boolean = false;



  constructor(private dataService: DataService,
    private userDataService: UserDataService,
    private cookiesService: CookiesService,
    private router: Router) { }

  ngOnInit() { }

  //open camera and scan the barcod for get the serial number
  scanBarcodForFirstLogin() { }

  login() {
    if (this.loginWithSerialNumber) {
      this.dataService.apiName = "login/serial-number";
      this.dataService.saveData(this.serialLoginNumber).subscribe((response) => {

        if (response.value) {
          //update user data service
          this.userDataService.userData = response.data;

          //connect the app and show page to select user name and password

          //if select remember me, save cookies
          if (this.rememberMe) {
            let cookieData = JSON.stringify(this.userDataService);
            this.cookiesService.setCookie("userData", cookieData, 0);
          }



          //for admenistrator
          if (this.userDataService.userData.userLevel == 1)
            this.router.navigate(['home-page-for-integrator']);

          //for integrator
          if (this.userDataService.userData.userLevel == 2)
            this.router.navigate(['home-page-for-tenant']);

          //for tenant
          if (this.userDataService.userData.userLevel == 5)
            this.router.navigate(['home-page-for-tenant']);
        }
      });
    }
    else {
      this.dataService.apiName = "login/user-name";
      this.dataService.saveData(this.loginUserData).subscribe((response) => {

        //check if response is valid user
        if (response.value) {
          //update user data service
          this.userDataService.userData = response.data;

          //if select remember me, save cookies
          if (this.rememberMe) {
            let cookieData = JSON.stringify(this.userDataService);
            this.cookiesService.setCookie("userData", cookieData, 0);
          }



          //for admenistrator
          if (this.userDataService.userData.userLevel == 1)
            this.router.navigate(['home-page-for-integrator']);

          //for integrator
          else if (this.userDataService.userData.userLevel == 2)
            this.router.navigate(['home-page-for-tenant']);

          //for tenant
          else if (this.userDataService.userData.userLevel == 5)
            this.router.navigate(['home-page-for-tenant']);
        }
      });


      //DELETE
      //this.router.navigate(['home-page-for-integrator']);
      this.router.navigate(['home-page-for-tenant']);

    }
  }

  changeLoginWay() {
    this.loginWithSerialNumber = !this.loginWithSerialNumber
  }

}
