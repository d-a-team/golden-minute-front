import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home-page-for-integrator',
  templateUrl: './home-page-for-integrator.component.html',
  styleUrls: ['./home-page-for-integrator.component.css']
})
export class HomePageForIntegratorComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  //
  goToAddNewDevice() {
    this.router.navigate(['add-device-page']);
  }

}
