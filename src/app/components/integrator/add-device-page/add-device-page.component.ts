import { SignControllerComponent } from './../../../shared/components/sign-controller/sign-controller.component';
import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api/menuitem';
import { DataFromDeviceService } from 'src/app/shared/services/data-from-device.service';
import { HttpErrorResponse } from '@angular/common/http';
import { LightData } from 'src/app/class/smartsignData';
declare var $: any;

@Component({
  selector: 'app-add-device-page',
  templateUrl: './add-device-page.component.html',
  styleUrls: ['./add-device-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AddDevicePageComponent implements OnInit {

  @ViewChild(SignControllerComponent, null) signControllerComponent: SignControllerComponent;

  public items: MenuItem[] = [
    { label: 'Coonect' },
    { label: 'Building' },
    { label: 'User' },
    { label: 'Finish' },
  ];
  public activeIndex: number = 0;

  public deviceName: string;
  public buildingText: string;
  public buildingNumber: number;
  public userName: string;
  public password: string;

  public loadingPage: boolean = false;
  public errorConnectToDevice: boolean = false;


  constructor(private dataFromDeviceService: DataFromDeviceService) { }

  ngOnInit() {
  }

  //
  connectToDevice() {
    $("#slide2").animate({ width: 'toggle' }, 350);
    this.activeIndex = 1;


    /*  this.loadingPage = true;
      //call to the device
      this.dataFromDeviceService.apiName = "db/db-forward";
      this.dataFromDeviceService.saveData(this.deviceName).subscribe((response) => {
        
  
        this.loadingPage = false;
        this.activeIndex = 1;
       // $("#slide2").animate({ width: 'toggle' }, 350);
      },
        (error: HttpErrorResponse) => {
          
          this.errorConnectToDevice = true;
          this.loadingPage = false;
        });
  */

  }

  //
  backConnectToDevice() {
    this.activeIndex = 1;
    $("#slide2").animate({ width: 'toggle' }, 350);
  }

  //
  saveBuildingDetails() {


    this.loadingPage = false;

    this.activeIndex = 2;
    $("#slide3").animate({ width: 'toggle' }, 350);
  }

  //
  backToBuildingDetails() {

  }

  //
  saveUserDetails() {


    this.loadingPage = false;

    this.activeIndex = 3;
    $("#slide4").animate({ width: 'toggle' }, 350);
  }





  public openLightModal() {
    let light: LightData;
    this.signControllerComponent.setColor(light);
  }

  public openBuildingNumberModal() {
    this.signControllerComponent.setBuildingNum(150);
  }

  public openTextAndFilesModal() {
    //this.signControllerComponent.setText("BLA BLA KKA");
  }
}
