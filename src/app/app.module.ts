import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HashLocationStrategy, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TenantSettingPageComponent } from './components/tenant/tenant-setting-page/tenant-setting-page.component';
import { HomePageForTenantComponent } from './components/tenant/home-page-for-tenant/home-page-for-tenant.component';
import { HomePageForIntegratorComponent } from './components/integrator/home-page-for-integrator/home-page-for-integrator.component';
import { AddDevicePageComponent } from './components/integrator/add-device-page/add-device-page.component';
import { SharedModule } from './shared/shared.module';
import { EnvServiceProvider } from './shared/services/env/env.service.provider';
import { TranslateComponent } from './shared/components/translate/translate.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SortablejsModule } from 'ngx-sortablejs';

//components
import { PersonalDetailsComponent } from './components/tenant/personal-details/personal-details.component';
import { FamilyDetailsComponent } from './components/tenant/family-details/family-details.component';
import { ContactUsComponent } from './components/tenant/contact-us/contact-us.component';
import { AlarmModalComponent } from './components/tenant/_tenant-modals/alarm-modal/alarm-modal.component';
import { BuildingNumberModalComponent } from './components/tenant/_tenant-modals/building-number-modal/building-number-modal.component';
import { CameraModalComponent } from './components/tenant/_tenant-modals/camera-modal/camera-modal.component';
import { LightModalComponent } from './components/tenant/_tenant-modals/light-modal/light-modal.component';
import { TextAndFilesModalComponent } from './components/tenant/_tenant-modals/text-and-files-modal/text-and-files-modal.component';
import { WifiModalComponent } from './components/tenant/_tenant-modals/wifi-modal/wifi-modal.component';
import { EmergencyModalComponent } from './components/tenant/_tenant-modals/emergency-modal/emergency-modal.component';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,

    //components
    TranslateComponent,

    /* Tenant */
    LoginPageComponent,
    HomePageForTenantComponent,
    TenantSettingPageComponent,
    PersonalDetailsComponent,
    FamilyDetailsComponent,
    ContactUsComponent,
    AlarmModalComponent,
    BuildingNumberModalComponent,
    CameraModalComponent,
    LightModalComponent,
    TextAndFilesModalComponent,
    WifiModalComponent,
    EmergencyModalComponent,


    /* integrator */
    HomePageForIntegratorComponent,
    AddDevicePageComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    SortablejsModule.forRoot({ animation: 150 }),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader), // exported factory function needed for AoT compilation
        deps: [HttpClient]
      }
    }),
    AppRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [EnvServiceProvider,
    { provide: LocationStrategy, useClass: PathLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
