import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cutTextPipe'
})
export class CutTextPipePipe implements PipeTransform {

  transform(text: any): any {
    let response = "";

    for (let i = 0; i < text.length && i <= 5; i++)
      response += text[i];

    response += " ...";


    return response;
  }

}
