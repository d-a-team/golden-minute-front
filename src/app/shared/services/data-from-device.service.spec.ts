/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DataFromDeviceService } from './data-from-device.service';

describe('Service: DataFromDevice', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataFromDeviceService]
    });
  });

  it('should ...', inject([DataFromDeviceService], (service: DataFromDeviceService) => {
    expect(service).toBeTruthy();
  }));
});
