import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EnvService {

  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
  public apiUrl = '';

  // Chat url
  public apiUrlForDevice = '';

  //webSocket url
  public webSocketUrl = '';

  public phoneNumber = '';
  public serialNumber = '';

  constructor() { }

}
