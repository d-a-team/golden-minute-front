import { Injectable } from '@angular/core';
import { UserData } from 'src/app/class/UserData';

@Injectable({
  providedIn: 'root'
})
export class UserDataService {

  public userData: UserData;

  constructor() {

    this.userData = {
      userName: "",
      userId: "",
      userLevel: null,
      deviceSerialNumber: ""
    }
  }

}
