import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EnvService } from './env/env.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

    private baseUrl: string;
    public apiName: string;

    constructor(private http: HttpClient,
        private envService: EnvService) {
        this.baseUrl = this.envService.apiUrl;
    }

    public getData(): Observable<any> {
        return this.http.get(this.baseUrl + this.apiName);
    }

    public getDataByParam(paramValue: string | number): Observable<any> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };

        return this.http.get(this.baseUrl + this.apiName + '/' + paramValue, httpOptions);
    }

    public getDataByObject(data: any): Observable<any> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };

        return this.http.post(this.baseUrl + this.apiName, JSON.stringify(data), httpOptions);
    }

    public saveData(data: any): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };

        return this.http.post(this.baseUrl + this.apiName, JSON.stringify(data), httpOptions);
    }

    public updateData(paramValue: string | number, data: any): Observable<any> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            }),
        };

        return this.http.put(this.baseUrl + this.apiName + '/' + paramValue, JSON.stringify(data), httpOptions);
    }

    public deleteData(data: any): Observable<any> {

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json; charset=utf-8',
            }),
        };

        return this.http.put(this.baseUrl + this.apiName, JSON.stringify(data), httpOptions);
    }

    public getApiName(): string {
        return this.apiName;
    }

    public setBaseUrl(baseUrl) {
        this.baseUrl = baseUrl;
    }

    public setBaseUrlToDefault() {
        this.baseUrl = this.envService.apiUrl;
    }

    public getBaseUrl() {
        return this.baseUrl;
    }
}

