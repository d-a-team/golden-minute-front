import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { EnvService } from './env/env.service';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})

export class WebSocketService {

  public ws = null;

  constructor(private envService: EnvService) { }


  //coonect to web socket
  //start connection with the socket of smart sign
  connectToWebSocket(serialNumber) {

    return new Promise((resolve, rejected) => {
      console.log('connecting to WS: ' + this.envService.webSocketUrl);

      this.ws = io(this.envService.webSocketUrl, {
        query: {
          serialNumber: serialNumber
        }
      });

      this.ws.on('connect', () => {
        console.log("Connected to websocket");
        let response = {
          status: true,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('error', (ex) => {
        console.log("Eroor: in connection to user socket, Ex: " + ex.message);

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('error', (err) => {
        console.log('Error connecting to server', err);

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('disconnect', () => {
        console.log('Disconnect from server');

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('reconnect', (number) => {
        console.log('Reconnected to server', number);

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('reconnect_attempt', () => {
        console.log('Reconnect Attempt');

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('reconnecting', (number) => {
        console.log('Reconnecting to server', number);

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('reconnect_error', (err) => {
        console.log('Reconnect Error', err);

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('reconnect_failed', () => {
        console.log('Reconnect failed');

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });

      this.ws.on('connect_error', (ex) => {
        console.log('connect_error, Error: ' + ex);

        let response = {
          status: false,
          ws: this.ws
        }
        resolve(response);
      });
    })
  }


  //when get new event
  /*   webSocket(data) {
      switch (data.type) {
        case "event": {
          console.log("new event arrivaed from sign");
  
          break;
        }
        case "alarm": {
          console.log("new alarm arrivaed from sign");
  
          break;
        }
        default: {
  
          break;
        }
      }
    } */

}
