import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoadingPageComponent } from './components/loading-page/loading-page.component';
import { PreviousComponent } from './components/previous/previous.component';
import { SignControllerComponent } from './components/sign-controller/sign-controller.component';


//prime ng
import { ButtonModule } from 'primeng/button';
import { StepsModule } from 'primeng/steps';
import { CheckboxModule } from 'primeng/checkbox';
import { FileUploadModule } from 'primeng/fileupload';
import { DropdownModule } from 'primeng/dropdown';
import { ToastModule } from 'primeng/toast';
import { TabMenuModule } from 'primeng/tabmenu';
import { CutTextPipePipe } from './pipes/CutTextPipe.pipe';

@NgModule({
  imports: [
    CommonModule,

    //primeNg
    StepsModule,
    CheckboxModule,
    ButtonModule,
    FileUploadModule,
    DropdownModule,
    ToastModule,
    TabMenuModule
  ],
  declarations: [
    LoadingPageComponent,
    PreviousComponent,
    SignControllerComponent,

    //pipes
    CutTextPipePipe
  ],
  exports: [
    LoadingPageComponent,
    PreviousComponent,
    SignControllerComponent,

    //primeNg
    StepsModule,
    CheckboxModule,
    ButtonModule,
    FileUploadModule,
    DropdownModule,
    ToastModule,
    TabMenuModule,

    //pipes
    CutTextPipePipe
  ]
})
export class SharedModule { }
