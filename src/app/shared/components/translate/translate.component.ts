import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { CookiesService } from '../../services/cookies.service';
declare var $: any;

@Component({
  selector: 'translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.css']
})
export class TranslateComponent implements OnInit { 

  public flagsImages = {
    he: "../../../assets/flags/israel.png",
    en: "../../../assets/flags/usa.png"
  }

  public currentLang: string;

  private toggleTranslateMenu: boolean = false;

  constructor(private translateService: TranslateService,
    private cookiesService: CookiesService) { }

  ngOnInit() {
    this.setUserLanguage();
  }

  private setUserLanguage() {
    this.currentLang = this.cookiesService.getCookie('lang');

    if (!this.currentLang) {
      this.currentLang = "he";
    }
  }

  // change the language and save it cookies and change the language button at the topnavbar menu
  public switchLanguage(language: string) {

    // change the lang of the website
    this.translateService.use(language);

    // save the lang in cookies
    this.cookiesService.setCookie('lang', language, 1);

    let headId = document.getElementById('rtlStyleSheet');

    //switch between rtl to ltr 
    if (language == "he") {
      $("body").removeClass("ltr");
      $("body").addClass("rtl");
      headId.setAttribute('href', '../../assets/bootstrap-rtl.min.css');
    }
    else {
      $("body").removeClass("rtl");
      $("body").addClass("ltr");
      headId.removeAttribute('href');
    }
    this.currentLang = language;
    this.toggleTranslateMenu = true;
    this.openOrCloseTranslateMenu();

    this.addClassStyleForTextSize();
  }

  public openOrCloseTranslateMenu() {

    if (!this.toggleTranslateMenu) {
      /*  if (this.currentLang == "he")
         $(".choose-lang").css({ "left": "-4px" });
       else */
      $(".choose-lang").css({ "right": "-4px" });
    }
    else {
      /*  if (this.currentLang == "he")
         $(".choose-lang").css({ "left": "-250px" });
       else */
      $(".choose-lang").css({ "right": "-250px" });
    }

    this.toggleTranslateMenu = !this.toggleTranslateMenu;
  }

  //add class for style of the text
  addClassStyleForTextSize() {
    if (this.currentLang == "he") {
      $("#bigText").removeClass("bigText_EN");
      $("#bigText2").removeClass("bigText2_EN");
      $("#text3").removeClass("text3_EN");
      $("#text3_1").removeClass("text3_EN");
      $("#textOfImgAvi").removeClass("textOfImgAvi_EN");
      $("#textOfImgAvi2").removeClass("textOfImgAvi2_EN");
      $("#peddingText5").removeClass("peddingText5_En");
      $("#bigText2Phone").removeClass("bigText2Phone_EN");
      $("#bigText2Phone1").removeClass("bigText2Phone_EN");
      $("#buttonText1").removeClass("buttonSize_EN");
      $("#buttonText2").removeClass("buttonSize_EN");
      $("#buttonText3").removeClass("buttonSize_EN");
      $("#buttonText4").removeClass("buttonSize_EN");
      $("#buttonText5").removeClass("buttonSize_EN");
      $("#buttonText6").removeClass("buttonSize_EN");
      $("#buttonText1").removeClass("buttonText_EN");
      $("#buttonText2").removeClass("buttonText_EN");
      $("#buttonText3").removeClass("buttonText_EN");
      $("#buttonText4").removeClass("buttonText_EN");
      $("#buttonText5").removeClass("buttonText_EN");
      $("#buttonText6").removeClass("buttonText_EN");
      $("#bigButtonText").removeClass("bigButtonText_EN");
      $("#bigButtonText").removeClass("bigButtonSize_EN");
    }
    else {
      $("#bigText").addClass("bigText_EN");
      $("#bigText2").addClass("bigText2_EN");
      $("#text3").addClass("text3_EN");
      $("#text3_1").addClass("text3_EN");
      $("#textOfImgAvi").addClass("textOfImgAvi_EN");
      $("#textOfImgAvi2").addClass("textOfImgAvi2_EN");
      $("#peddingText5").addClass("peddingText5_En");
      $("#bigText2Phone").addClass("bigText2Phone_EN");
      $("#bigText2Phone1").addClass("bigText2Phone_EN");
      $("#buttonText1").addClass("buttonSize_EN");
      $("#buttonText2").addClass("buttonSize_EN");
      $("#buttonText3").addClass("buttonSize_EN");
      $("#buttonText4").addClass("buttonSize_EN");
      $("#buttonText5").addClass("buttonSize_EN");
      $("#buttonText6").addClass("buttonSize_EN");
      $("#buttonText1").addClass("buttonText_EN");
      $("#buttonText2").addClass("buttonText_EN");
      $("#buttonText3").addClass("buttonText_EN");
      $("#buttonText4").addClass("buttonText_EN");
      $("#buttonText5").addClass("buttonText_EN");
      $("#buttonText6").addClass("buttonText_EN");
      $("#bigButtonText").addClass("bigButtonText_EN");
      $("#bigButtonText").addClass("bigButtonSize_EN");
    }
  }

}
