import { LightData, ScreenData } from 'src/app/class/smartsignData';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-sign-controller',
  templateUrl: './sign-controller.component.html',
  styleUrls: ['./sign-controller.component.css']
})
export class SignControllerComponent implements OnInit {

  @Input() clickable = true;
  @Output() setColorClicked = new EventEmitter();
  @Output() setBuildingNumClicked = new EventEmitter();
  @Output() setTextClicked = new EventEmitter();

  signIcon: SVGImageElement;
  lightType: 'A' | 'B' | 'C';
  lightColor1: SVGStopElement; // #b21f24
  lightColor2: SVGStopElement; // #461313 
  lightColor11: SVGStopElement; // #b21f24
  lightColor22: SVGStopElement; // #461313 
  textRowsCount: number = 1;
  imageContent: SVGImageElement;
  videoContent: SVGGElement;
  buildingNumData: { number: number, subText: string } = { number: 18, subText: '' }

  private sleepTimeout;

  private lastContent;

  constructor() { }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.signIcon = $('#signIcon');
    this.lightColor1 = $('#light-color-1');
    this.lightColor2 = $('#light-color-2');
    this.lightColor11 = $('#light-color-11');
    this.lightColor22 = $('#light-color-22');
    this.imageContent = $('#image-content');

    this.clearContent();
  }

  public onSetColorClicked(event?) {
    if (event) {
      event.preventDefault();
      event.stopPropagation();
    }
    if (this.clickable)
      this.setColorClicked.emit();
  }

  public onSetBuildingNumClicked() {
    if (this.clickable)
      this.setBuildingNumClicked.emit();
  }

  public onSetTextClicked() {
    if (this.clickable)
      this.setTextClicked.emit();
  }

  private clearContent() {
    console.log(new Date().toString() + ' clearContent()');
    if (!this.lastContent) {
      $('.svgIcon-text-content').hide().html('');
      $(this.imageContent).hide().attr('href', '');
      $('#video-content foreignObject').hide().html('');
    }
    else if (this.lastContent.exitAnimationType == 'normal') {
      // text
      if (this.lastContent.type == 'Text') {
        $('.svgIcon-text-content').hide(this.lastContent.exitAnimationDuration * 1000, function () {
          $(this).html('');
        });
      }

      // image
      else if (this.lastContent.type == 'Image') {
        $(this.imageContent).fadeTo(this.lastContent.exitAnimationDuration * 1000, 0, function () {
          $(this).hide().attr('href', '');
          $(this).hide().css('opacity', 1);
        });
      }

      // video
      else if (this.lastContent.type == 'Video') {
        $('#video-content foreignObject').fadeTo(this.lastContent.exitAnimationDuration * 1000, 0, function () {
          $(this).hide().html('');
          $(this).hide().css('opacity', 1);
        });
      }
    }
    else if (this.lastContent.exitAnimationType == 'ltr') {
      /* TEXT */
      if (this.lastContent.type == 'Text') {
        $('.svgIcon-text-content').css('transition', 'all ' + (this.lastContent.exitAnimationDuration * 1000) + 'ms ease-in-out');
        $('.svgIcon-text-content').attr('transform', "translate(" + 2406 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $('.svgIcon-text-content').css('transition', '')
            .attr('x', 1203)
            .removeAttr('transform')
            .hide();
        }, this.lastContent.exitAnimationDuration * 1000);
      }
      /* Image */
      else if (this.lastContent.type == 'Image') {
        $(this.imageContent).css('transition', 'all ' + (this.lastContent.entranceAnimationDuration * 1000) + 'ms ease-in-out');
        $(this.imageContent).attr('transform', "translate(" + 2386 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $(this.imageContent).css('transition', '')
            .attr('x', 21)
            .removeAttr('transform')
            .hide();
        }, this.lastContent.entranceAnimationDuration * 1000);
      }
      /* Video */
      else if (this.lastContent.type == 'Video') {
        $('#video-content foreignObject').css('transition', 'all ' + (this.lastContent.entranceAnimationDuration * 1000) + 'ms ease-in-out');
        $('#video-content foreignObject').attr('transform', "translate(" + 2386 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $('#video-content foreignObject').css('transition', '')
            .attr('x', 21)
            .removeAttr('transform')
            .html('')
            .hide();
        }, this.lastContent.entranceAnimationDuration * 1000);
      }
    }
    else if (this.lastContent.exitAnimationType == 'rtl') {
      /* Text */
      if (this.lastContent.type == 'Text') {
        $('.svgIcon-text-content').css('transition', 'all ' + (this.lastContent.exitAnimationDuration * 1000) + 'ms ease-in-out');
        $('.svgIcon-text-content').attr('transform', "translate(" + -2406 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $('.svgIcon-text-content').css('transition', '')
            .attr('x', 1203)
            .removeAttr('transform')
            .hide();
        }, this.lastContent.exitAnimationDuration * 1000);
      }
      else if (this.lastContent.type == 'Image') {
        $(this.imageContent).css('transition', 'all ' + (this.lastContent.entranceAnimationDuration * 1000) + 'ms ease-in-out');
        $(this.imageContent).attr('transform', "translate(" + -2365 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $(this.imageContent).css('transition', '')
            .attr('x', 21)
            .removeAttr('transform')
            .hide()
        }, this.lastContent.entranceAnimationDuration * 1000);
      }
      else if (this.lastContent.type == 'Video') {
        $('#video-content foreignObject').css('transition', 'all ' + (this.lastContent.entranceAnimationDuration * 1000) + 'ms ease-in-out');
        $('#video-content foreignObject').attr('transform', "translate(" + -2365 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $('#video-content foreignObject').css('transition', '')
            .attr('x', 21)
            .removeAttr('transform')
            .html('')
            .hide()
        }, this.lastContent.entranceAnimationDuration * 1000);
      }
    }
  }



  /*********************************************/
  //                   Text                    //
  /*********************************************/
  public async setText(textData: ScreenData) {
    console.log(new Date().toString() + '  setText()');
    this.lastContent = textData;
    let textRows = textData.text.split("\n");
    this.textRowsCount = textRows.length;

    await this.sleep(100);

    let textContent = $('.svgIcon-text-content')
      .attr('font-size', +textData.fontSize * 3.5)
      .attr('font-family', textData.font)
      .attr('fill', textData.color);
    /* 
        console.log('rows count: ' + this.textRowsCount); */
    $(textContent[0]).html(textRows[0]);

    if (this.textRowsCount >= 2)
      $(textContent[1]).html(textRows[1]);

    if (this.textRowsCount == 3)
      $(textContent[2]).html(textRows[2]);


    if (textData.entranceAnimationType == 'normal')
      $('.svgIcon-text-content').show(textData.entranceAnimationDuration * 1000);

    else if (textData.entranceAnimationType == 'ltr') {
      $('.svgIcon-text-content').attr('x', -1203).show();
      $('.svgIcon-text-content').css('transition', 'all ' + (textData.entranceAnimationDuration * 1000) + 'ms ease-in-out');
      $('.svgIcon-text-content').attr('transform', "translate(" + 2406 + ")");

      // reset the attributes to normal when animation is done
      setTimeout(() => {
        $('.svgIcon-text-content').css('transition', '');
        $('.svgIcon-text-content').attr('x', 1203);
        $('.svgIcon-text-content').removeAttr('transform');
      }, textData.entranceAnimationDuration * 1000);
    }

    else if (textData.entranceAnimationType == 'rtl') {
      $('.svgIcon-text-content').attr('x', 3609).show();
      $('.svgIcon-text-content').css('transition', 'all ' + (textData.entranceAnimationDuration * 1000) + 'ms ease-in-out');
      $('.svgIcon-text-content').attr('transform', "translate(" + -2406 + ")");

      // reset the attributes to normal when animation is done
      setTimeout(() => {
        $('.svgIcon-text-content').css('transition', '');
        $('.svgIcon-text-content').attr('x', 1203);
        $('.svgIcon-text-content').removeAttr('transform');
      }, textData.entranceAnimationDuration * 1000);
    }

    setTimeout(() => {
      this.clearContent();
    }, (this.lastContent.entranceAnimationDuration + this.lastContent.duration) * 1000);
  }


  /*********************************************/
  //                  Image                    //
  /*********************************************/
  public setImage(imageData) {
    console.log(new Date().toString() + ' setImage()');
    this.lastContent = imageData;
    $(this.imageContent).attr('href', imageData.image);

    if (imageData.entranceAnimationType == 'normal') {
      $(this.imageContent).css('opacity', 0).show();
      $(this.imageContent).fadeTo(imageData.entranceAnimationDuration * 1000, 1);
    }

    else if (imageData.entranceAnimationType == 'ltr') {
      $(this.imageContent).attr('x', -2386).show();
      $(this.imageContent).css('transition', 'all ' + (imageData.entranceAnimationDuration * 1000) + 'ms ease-in-out');
      $(this.imageContent).attr('transform', "translate(" + 2386 + ")");

      // reset the attributes to normal when animation is done
      setTimeout(() => {
        $(this.imageContent).css('transition', '');
        $(this.imageContent).attr('x', 21);
        $(this.imageContent).removeAttr('transform');
      }, imageData.entranceAnimationDuration * 1000);
    }

    else if (imageData.entranceAnimationType == 'rtl') {
      $(this.imageContent).attr('x', 2386).show();
      $(this.imageContent).css('transition', 'all ' + (imageData.entranceAnimationDuration * 1000) + 'ms ease-in-out');
      $(this.imageContent).attr('transform', "translate(" + -2365 + ")");

      // reset the attributes to normal when animation is done
      setTimeout(() => {
        $(this.imageContent).css('transition', '');
        $(this.imageContent).attr('x', 21);
        $(this.imageContent).removeAttr('transform');
      }, imageData.entranceAnimationDuration * 1000);
    }


    setTimeout(() => {
      this.clearContent();
    }, (this.lastContent.entranceAnimationDuration + this.lastContent.duration) * 1000);
  }


  /*********************************************/
  //                  Video                    //
  /*********************************************/
  public setVideo(videoData) {
    console.log(new Date().toString() + ' setVideo()');
    // create video element and set its source
    $('#video-content').html(`<foreignObject style="display:none" height="580" width="2365" x="21" y="21">
    <video loop autoplay width="100%" height="100%">
    <source src="${videoData.video}" type="video/mp4">
    </video>
    </foreignObject>`);

    $(document).ready(() => {
      ;
      if (videoData.entranceAnimationType == 'normal') {
        $('#video-content foreignObject').css('opacity', 0).show();
        $('#video-content foreignObject').fadeTo(videoData.entranceAnimationDuration * 1000, 1);
      }

      else if (videoData.entranceAnimationType == 'ltr') {
        $('#video-content foreignObject').attr('x', -2386).show();
        $('#video-content foreignObject').css('transition', 'all ' + (videoData.entranceAnimationDuration * 1000) + 'ms ease-in-out');
        $('#video-content foreignObject').attr('transform', "translate(" + 2386 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $('#video-content foreignObject').css('transition', '');
          $('#video-content foreignObject').attr('x', 21);
          $('#video-content foreignObject').removeAttr('transform');
        }, videoData.entranceAnimationDuration * 1000);
      }

      else if (videoData.entranceAnimationType == 'rtl') {
        $('#video-content foreignObject').attr('x', 2386).show();
        $('#video-content foreignObject').css('transition', 'all ' + (videoData.entranceAnimationDuration * 1000) + 'ms ease-in-out');
        $('#video-content foreignObject').attr('transform', "translate(" + -2365 + ")");

        // reset the attributes to normal when animation is done
        setTimeout(() => {
          $('#video-content foreignObject').css('transition', '');
          $('#video-content foreignObject').attr('x', 21);
          $('#video-content foreignObject').removeAttr('transform');
        }, videoData.entranceAnimationDuration * 1000);
      }

      setTimeout(() => {
        this.clearContent();
      }, (this.lastContent.entranceAnimationDuration + this.lastContent.duration) * 1000);
    })
  }


  /*********************************************/
  //              Building number              //
  /*********************************************/
  public async setBuildingNum(buildingNumberData) {
    this.buildingNumData = buildingNumberData;

    $(document).ready(() => {
      // get element
      let buidingNumber = $('.svgIcon-building-content');

      let newNumberTextSize = 355 - 30 * buildingNumberData.number.toString().length
      // set font size based on the number of chars
      $(buidingNumber[0]).attr('font-size', newNumberTextSize > 100 ? newNumberTextSize : 100).html(buildingNumberData.number);

      if (buildingNumberData.subText) {
        let newSubTextSize = 355 - 30 * buildingNumberData.subText.toString().length;
        $(buidingNumber[1]).attr('font-size', newSubTextSize > 100 ? newSubTextSize : 100).html(buildingNumberData.subText);
      }
    })
  }


  /*********************************************/
  //                   Light                   //
  /*********************************************/
  public async setColor(light: LightData) {

    this.lightType = light.type;

    if (this.sleepTimeout)
      clearTimeout(this.sleepTimeout);

    let x = Number(light.blinkTime);
    let minutes = light.duration * 60 * 1000;
    let date = new Date(new Date().getTime() + minutes);
    let flag = true;

    if (light.state) {
      //blinking sate
      if (light.blinkState)
        while (new Date().getTime() - date.getTime() < 0) {

          if (flag) {
            if (this.lightType == 'A') {
              $(this.lightColor1).attr('stop-color', light.color);
              $(this.lightColor2).attr('stop-color', light.color);
            }
            else {
              $(this.lightColor11).attr('stop-color', light.color);
              $(this.lightColor22).attr('stop-color', light.color);
            }
          }
          else {
            if (this.lightType == 'A') {
              $(this.lightColor1).attr('stop-color', '#b21f24');
              $(this.lightColor2).attr('stop-color', '#461313');
            }
            else {
              $(this.lightColor11).attr('stop-color', '#00000000');
              $(this.lightColor22).attr('stop-color', '#00000000');
            }
          }

          flag = !flag;
          await this.sleep(x);
        }
      else {
        if (this.lightType == 'A') {
          $(this.lightColor1).attr('stop-color', light.color);
          $(this.lightColor2).attr('stop-color', light.color);
        }
        else {
          $(this.lightColor11).attr('stop-color', light.color);
          $(this.lightColor22).attr('stop-color', light.color);
        }
      }
    }
    else {
      if (this.lightType == 'A') {
        $(this.lightColor1).attr('stop-color', '#b21f24');
        $(this.lightColor2).attr('stop-color', '#461313');
      }
      else {
        $(this.lightColor11).attr('stop-color', '#00000000');
        $(this.lightColor22).attr('stop-color', '#00000000');
      }
    }

    /*  else {
       $(this.lightColor1).attr('stop-color', color);
       $(this.lightColor2).attr('stop-color', color);
 
       await this.sleep(x);
 
       $(this.lightColor1).attr('stop-color', '#b21f24');
       $(this.lightColor2).attr('stop-color', '#461313');
 
     } */

    /* 
        if (color) {
          $(this.lightColor1).attr('stop-color', color);
          $(this.lightColor2).attr('stop-color', color);
        }
        else {
          $(this.lightColor1).attr('stop-color', '#b21f24');
          $(this.lightColor2).attr('stop-color', '#461313');
        } */
  }


  sleep(duration) {
    return new Promise((resolve) => {
      this.sleepTimeout = setTimeout(() => {
        resolve();
      }, duration)
    })
  }






  /****************************************************/
  //                   Clear screen                   //
  /****************************************************/



}
