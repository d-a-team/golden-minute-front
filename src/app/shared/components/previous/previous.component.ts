import { Router } from '@angular/router';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-previous',
  templateUrl: './previous.component.html',
  styleUrls: ['./previous.component.css']
})
export class PreviousComponent implements OnInit {

  @Output() backButton = new EventEmitter();
  @Input() type: number;

  constructor(private location: Location,
    private route: Router) { }

  ngOnInit() {
  }

  public previousRoute() {

    this.backButton.emit("pressed");

    this.location.back();

    //  if (this.type == 1)
    //this.route.navigate(['/']);

  }

}
